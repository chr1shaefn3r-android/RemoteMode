package de.christophhaefner.android.remotemode.service;

import android.app.KeyguardManager;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import java.security.Key;

public class RemoteModeService extends Service {

    private PowerManager.WakeLock mWakeLock = null;
    private KeyguardManager.KeyguardLock mKeyguardLock = null;

    public RemoteModeService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("RemoteMode", "[RemoteModeService][onCreate]");
        PowerManager pm = (PowerManager)getSystemService(POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "RemoteModeWackeLogTag");
        mWakeLock.acquire();
        KeyguardManager km = (KeyguardManager)getSystemService(KEYGUARD_SERVICE);
        mKeyguardLock = km.newKeyguardLock("RemoteMode");
        mKeyguardLock.disableKeyguard();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("RemoteMode", "[RemoteModeService][onDestroy]");
        mWakeLock.release();
        mKeyguardLock.reenableKeyguard();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new Binder();
    }
}
