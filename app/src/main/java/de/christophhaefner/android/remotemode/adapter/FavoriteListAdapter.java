package de.christophhaefner.android.remotemode.adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.graphics.Palette;

import de.christophhaefner.android.remotemode.R;
import de.christophhaefner.android.remotemode.favorites.FavoriteDeleter;
import de.christophhaefner.android.remotemode.favorites.FavoriteEntry;

public class FavoriteListAdapter extends CursorAdapter {

    private final LayoutInflater mInflater;

    public FavoriteListAdapter(Context context) {
        super(context, null, 0);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View rootView = mInflater.inflate(R.layout.list_item_favorite, parent, false);
        ViewCache viewCache = new ViewCache();
        viewCache.favoriteLabel = (TextView)rootView.findViewById(R.id.favoriteText);
        viewCache.favoriteApplicationIcon = (ImageView)rootView.findViewById(R.id.favoriteApplicationIcon);
        viewCache.favoriteDelete = (ImageButton)rootView.findViewById(R.id.favoriteDelete);
        rootView.setTag(viewCache);
        return rootView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        FavoriteEntry favoriteEntry = new FavoriteEntry(cursor.getString(1), context.getPackageManager());
        final ViewCache viewCache = (ViewCache) view.getTag();
        viewCache.favoriteLabel.setText(favoriteEntry.getLabel());
        Drawable drawable = favoriteEntry.getApplicationIcon();
        viewCache.favoriteApplicationIcon.setImageDrawable(drawable);
        final FavoriteDeleter fd = new FavoriteDeleter(context, cursor.getInt(0));
        viewCache.favoriteDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fd.delete();
            }
        });
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        Palette.generateAsync(bitmap, new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                Palette.Swatch vibrant = palette.getVibrantSwatch();
                if (vibrant != null) {
                    // If we have a vibrant color
                    // update the title TextView
                    viewCache.favoriteLabel.setBackgroundColor(vibrant.getRgb());
                    viewCache.favoriteLabel.setTextColor(vibrant.getTitleTextColor());
                }
            }
        });
    }

    final static class ViewCache {
        public TextView favoriteLabel;
        public ImageView favoriteApplicationIcon;
        public ImageButton favoriteDelete;
    }
}
