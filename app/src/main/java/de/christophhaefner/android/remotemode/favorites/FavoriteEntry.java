package de.christophhaefner.android.remotemode.favorites;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;

public class FavoriteEntry {

    private String mPackageName;
    private PackageManager mPackageManager;
    private String mLabel = null;
    private Drawable mApplicationIcon = null;

    public FavoriteEntry(String packageName, PackageManager packageManager) {
        this.mPackageName = packageName;
        this.mPackageManager = packageManager;
    }

    public FavoriteEntry(Cursor cursor, PackageManager packageManager) {
        this.mPackageName = cursor.getString(cursor.getColumnIndex(Favorites.PACKAGE_NAME));
        this.mPackageManager = packageManager;
    }

    public String getLabel() {
        if(TextUtils.isEmpty(this.mLabel)) {
            try {
            ApplicationInfo appInfo = mPackageManager.getApplicationInfo(mPackageName, 0);
            CharSequence label = appInfo.loadLabel(mPackageManager);
            this.mLabel = String.valueOf(label);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                this.mLabel = "";
            }
        }
        return mLabel;
    }

    public Drawable getApplicationIcon() {
        if(null == mApplicationIcon) {
            try {
                mApplicationIcon = mPackageManager.getApplicationIcon(mPackageName);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        }
        return mApplicationIcon;
    }

    public String getPackageName() {
        return mPackageName;
    }
}
