package de.christophhaefner.android.remotemode.favorites;

import android.net.Uri;
import android.provider.BaseColumns;

public class Favorites implements BaseColumns {

    public static final Uri CONTENT_URI = Uri.parse("content://" + FavoritesContentProvider.AUTHORITY + "/favorites");
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.de.christophhaefner.android.remotemode.favorites";

    public static final String TABLE = "favorites";
    public static final String PACKAGE_NAME = "packageName";

}
