package de.christophhaefner.android.remotemode.favorites;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class FavoritesContentProvider extends ContentProvider {

    public static final String AUTHORITY = "de.christophhaefner.android.remotemode.FavoritesProvider";
    private static final int MATCH_FAVORITES_TABLE = 1;

    private static final UriMatcher mUriMatcher;
    static {
        mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mUriMatcher.addURI(AUTHORITY, Favorites.TABLE, MATCH_FAVORITES_TABLE);
    }

    private FavoritesDatabaseHelper mDBHelper;

    public FavoritesContentProvider() {
    }

    @Override
    public boolean onCreate() {
        mDBHelper = new FavoritesDatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        checkIfKnownUri(uri);
        SQLiteDatabase db = mDBHelper.getReadableDatabase();
        Cursor cursor = db.query(Favorites.TABLE, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        checkIfKnownUri(uri);
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        int count = db.delete(Favorites.TABLE, selection, selectionArgs);
        notifyChange(uri);
        return count;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        checkIfKnownUri(uri);
        SQLiteDatabase db = mDBHelper.getWritableDatabase();

        long rowId = db.insert(Favorites.TABLE, Favorites.PACKAGE_NAME, values);
        if(rowId > 0) {
            Uri noteUri = ContentUris.withAppendedId(Favorites.CONTENT_URI, rowId);
            notifyChange(noteUri);
            return noteUri;
        }

        throw new SQLException("Failed to insert row into " + uri);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        checkIfKnownUri(uri);
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        int count = db.update(Favorites.TABLE, values, selection, selectionArgs);
        notifyChange(uri);
        return count;
    }

    @Override
    public String getType(Uri uri) {
        checkIfKnownUri(uri);
        return Favorites.CONTENT_TYPE;
    }

    private void checkIfKnownUri(Uri uri) {
        if(mUriMatcher.match(uri) != MATCH_FAVORITES_TABLE) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    private void notifyChange(Uri uri) {
        getContext().getContentResolver().notifyChange(uri, null);
    }
}
