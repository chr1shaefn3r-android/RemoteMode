package de.christophhaefner.android.remotemode;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import de.christophhaefner.android.remotemode.activities.ChooseFavorite;
import de.christophhaefner.android.remotemode.favorites.FavoriteEntry;
import de.christophhaefner.android.remotemode.favorites.Favorites;
import de.christophhaefner.android.remotemode.fragments.FavoritesFragment;
import de.christophhaefner.android.remotemode.service.RemoteModeService;


public class MainActivity extends Activity implements FavoritesFragment.OnFragmentInteractionListener {

    private int mFavoriteRequestId = 21;
    private static final String INTENT_KEY_NOTIFICATION_ID = "NotificationId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int notificationId = getIntent().getIntExtra(INTENT_KEY_NOTIFICATION_ID, -1);
        if(notificationId > 0) {
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(notificationId);
            stopService(new Intent(this, RemoteModeService.class));
        }

        Button button = (Button) findViewById(R.id.buttonChooseFavorite);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(MainActivity.this, ChooseFavorite.class), mFavoriteRequestId);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == mFavoriteRequestId &&
                resultCode == RESULT_OK) {
            String packageName = data.getStringExtra(ChooseFavorite.BUNDLE_EXTRA_PACKAGE_NAME);
            FavoriteEntry favoriteEntry = new FavoriteEntry(packageName, getPackageManager());
            Log.d("RemoteMode", "[MainActivity][onActivityResult] appInfo: [" + favoriteEntry.getLabel() + " ("+packageName+")]");
            ContentValues values = new ContentValues();
            values.put(Favorites.PACKAGE_NAME, packageName);
            getContentResolver().insert(Favorites.CONTENT_URI, values);
        }
    }

    @Override
    public void onFragmentInteraction(FavoriteEntry favoriteEntry) {
        final int id = 42;
        // Creates an explicit intent for an Activity in your app
        Intent touchIntent = new Intent(this, MainActivity.class);
        touchIntent.putExtra(INTENT_KEY_NOTIFICATION_ID, id);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(touchIntent);
        PendingIntent pendingTouchIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this)
                .setOngoing(true)
                .setContentText("Tap to close")
                .setContentTitle("Remote Mode active")
                .setContentInfo(favoriteEntry.getLabel())
                .setSmallIcon(R.drawable.ic_stat_notification)
                .setTicker("Remote Mode activated (" + favoriteEntry.getLabel() + ")")
                .setContentIntent(pendingTouchIntent)
                .build();
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // id allows you to update the notification later on.
        mNotificationManager.notify(id, notification);

        Log.d("RemoteMode", "[MainActivity] calling startService");
        startService(new Intent(this, RemoteModeService.class));

        Log.d("RemoteMode", "[MainActivity] Start Intent");
        PackageManager pm = getApplicationContext().getPackageManager();
        Intent toBeStarted = pm.getLaunchIntentForPackage(favoriteEntry.getPackageName());
        startActivity(toBeStarted);
    }
}
