package de.christophhaefner.android.remotemode.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import de.christophhaefner.android.remotemode.R;
import de.christophhaefner.android.remotemode.fragments.AppListFragment;

public class ChooseFavorite extends Activity implements AppListFragment.OnFragmentInteractionListener {

    public static final String BUNDLE_EXTRA_PACKAGE_NAME = "packagename";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_favorite);
    }

    public void onFragmentInteraction(String packageName) {
        Intent data = new Intent();
        data.putExtra(ChooseFavorite.BUNDLE_EXTRA_PACKAGE_NAME, packageName);
        setResult(RESULT_OK, data);
        finish();
    }
}
