package de.christophhaefner.android.remotemode.applist;

import java.text.Collator;
import java.util.Comparator;

public class AlphaComparator implements Comparator<AppEntry> {

    private final Collator sCollator = Collator.getInstance();

    @Override
    public int compare(AppEntry object1, AppEntry object2) {
        return sCollator.compare(object1.getLabel(), object2.getLabel());
    }

}
