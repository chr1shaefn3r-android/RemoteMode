package de.christophhaefner.android.remotemode.favorites;

import android.content.Context;

public class FavoriteDeleter {

    private Context mContext;
    private int mId;

    public FavoriteDeleter(Context mContext, int id) {
        this.mContext = mContext;
        this.mId = id;
    }

    public void delete() {
        mContext.getContentResolver().delete(Favorites.CONTENT_URI, Favorites._ID + " = ?", new String[]{String.valueOf(mId)});
    }

    public int getId() {
        return mId;
    }
}
