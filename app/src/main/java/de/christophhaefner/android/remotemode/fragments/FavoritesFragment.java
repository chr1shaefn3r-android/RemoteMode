package de.christophhaefner.android.remotemode.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.christophhaefner.android.remotemode.R;
import de.christophhaefner.android.remotemode.adapter.FavoriteListAdapter;
import de.christophhaefner.android.remotemode.favorites.FavoriteEntry;
import de.christophhaefner.android.remotemode.favorites.Favorites;

public class FavoritesFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, AbsListView.OnItemClickListener {

    private OnFragmentInteractionListener mListener;
    private FavoriteListAdapter mAdapter;
    private AbsListView mListView;
    private LinearLayout mProgressContainer;
    private TextView mEmptyView;
    private boolean mListShown;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FavoritesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new FavoriteListAdapter(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);

        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        mListView.setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        mProgressContainer = (LinearLayout) view.findViewById(R.id.progressContainer);
        mEmptyView = (TextView) view.findViewById(android.R.id.empty);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Give some text to display if there is no data.  In a real
        // application this would come from a resource.
        setEmptyText("No favorites");

        // Start out with a progress indicator.
        setListShown(false);

        // Prepare the loader.  Either re-connect with an existing one,
        // or start a new one.
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // This is called when a new Loader needs to be created. We
        // only have one Loader, so we don't care about the ID.
        Uri baseUri = Favorites.CONTENT_URI;

        return new CursorLoader(getActivity(), baseUri, null, null, null, Favorites._ID);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // Swap the new cursor in
        mAdapter.swapCursor(data);

        // The list should now be shown.
        setListShown(true);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // Clear the data in the adapter.
        mAdapter.swapCursor(null);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            FavoriteEntry favoriteEntry = new FavoriteEntry((Cursor) mAdapter.getItem(position), getActivity().getPackageManager());
            mListener.onFragmentInteraction(favoriteEntry);
        }
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(FavoriteEntry favoriteEntry);
    }

    private void setListShown(boolean shown) {
        if (mListShown == shown) {
            return;
        }
        mListShown = shown;
        if (shown) {
            mProgressContainer.setVisibility(View.GONE);
            mListView.setVisibility(View.VISIBLE);
        } else {
            mProgressContainer.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
        }

    }

    private void setEmptyText(String text) {
        mEmptyView.setText(text);
        mListView.setEmptyView(mEmptyView);
    }
}
